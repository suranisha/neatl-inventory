<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Category;
use App\Models\Inventory_type;
use App\Models\Item;
use Illuminate\Database\Seeder;
use \App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this->DefaultUser();
        $this->CreateInventoryType();
    }

    public function DefaultUser(){
        $User = new User();
        $User->name = "Shahbaz Surani";
        $User->email = "shahbazsurani@gmail.com";
        $User->password = "Shahbazsurani";
        $User->save();
    }

    public function CreateInventoryType(){
        $inventoryType = ['Inventory', 'Delivery'];
        foreach ($inventoryType as $inv){
            $invObj = new Inventory_type();
            $invObj->type = $inv;
            $invObj->save();
        }
    }
}
