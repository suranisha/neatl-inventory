<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreInventory;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\inventory_count;
use App\Models\Inventory_type;
use App\Models\Item;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Inventory = Inventory::all()
            ->where('isActive',true);

        return view('inventory.index', ['Inventories'=>$Inventory]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $allItems = Item::all()->where('isActive',true);
        $inventoryTypeId = Inventory_type::all();

        $itemsByCategory = [];

        foreach ($allItems as $item){
            $itemCategoryName = $item->category->name;
            if(!array_key_exists($itemCategoryName, $itemsByCategory)){
                $itemsByCategory[$itemCategoryName] = [];
            }
            $itemsByCategory[$itemCategoryName][] = $item;
        }


        return view('inventory.create')
            ->with([
                'ItemsByCategory' => $itemsByCategory,
                'InventoryType' => $inventoryTypeId
            ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreInventory $request)
    {
        //validation
        $validated = $request->validated();

        $inventoryObj = new Inventory();
        $inventoryObj->InventoryDate = $validated['InventoryDate'];

        $InvTypeObj = Inventory_type::findOrFail($validated['inventoryType_id']);

        $inventoryObj->inventory_type()->associate($InvTypeObj);

        foreach ($request->all() as $key=>$value){
            $InventoryCountValue = (int)$value;
            if($InventoryCountValue<=0){
                continue;
            }

            if($inventoryObj->id === null){
                $inventoryObj->save();
            }
            if(is_int($key)){
                try {
                    $Item = Item::findOrFail($key);

                    $InventoryCount = new inventory_count();
                    $InventoryCount->item_count = $InventoryCountValue;
                    $InventoryCount->item()->associate($Item);
                    $InventoryCount->inventory()->associate($inventoryObj);
                    $InventoryCount->save();
                }catch (\Exception $e) {

                    return $e->getMessage();
                }
            }
        }
        return redirect()->route('inventory.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $allItems = Item::all()->where('isActive',true);
        $inventoryTypeId = Inventory_type::all();
        $inventory = inventory::findOrFail($id);

        if(!$inventory->isActive){
            return view('inventory.index');
        }

        $invCounts = $inventory->inventory_count;

        $invCountsByItemId = [];

        foreach ($invCounts as $invCount){
            $invCountsByItemId[$invCount->item->id] = $invCount;
        }

        $itemsByCategory = [];

        foreach ($allItems as $item){
            $itemCategoryName = $item->category->name;
            if(!array_key_exists($itemCategoryName, $itemsByCategory)){
                $itemsByCategory[$itemCategoryName] = [];
            }
            $itemsByCategory[$itemCategoryName][] = $item;
        }

        return view('inventory.edit')
            ->with([
                'ItemsByCategory' => $itemsByCategory,
                'InventoryType' => $inventoryTypeId,
                'inventory' => $inventory,
                'invCountsByItemId' => $invCountsByItemId
            ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //validation

        $inventory = Inventory::findOrFail($id);
        $inventory_countObj = Inventory::findOrFail($id)->inventory_count;

        foreach ($inventory_countObj as $C){
            $C->isActive = false;
            $C->delete();
        }

        $AllSubmittedFields = $request->input();

        foreach ($AllSubmittedFields as $FieldItemid => $Field){
            try {
                if(is_int($FieldItemid)){
                    $item = Item::findOrFail($FieldItemid);
                    $Count = (int)$Field;
                    if($Count<=0){
                        continue;
                    }

                    $InventoryCount = new inventory_count();
                    $InventoryCount->item_count = $Field;
                    $InventoryCount->item()->associate($item);
                    $InventoryCount->inventory()->associate($inventory);
                    $InventoryCount->save();

                }
            } catch (\Exception $e) {
            }
        }
        return redirect()->route('inventory.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $inventory = Inventory::findOrFail($id);
        $inventory->isActive=false;
        $inventory->save();

        return redirect()->route('inventory.index');

    }
}
