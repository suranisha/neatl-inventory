<?php

namespace App\Http\Controllers;

use App\Models\Inventory;
use App\Models\Report\ByInventoryDate;
use App\Models\Report\InventoryCountModelForReport;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Inventories = Inventory::all()
            ->where('isActive', true)
            ->sortByDesc('InventoryDate')
            ->sortByDesc('id');


        $InventoryUsedData = [];
        $ByInventoryDate = new ByInventoryDate();

        $shouldAddInEnd = true;

        //First Commplete all inventory Reports
        foreach ($Inventories as $Inventory){
            $IsInventoryTypeDelivery = $Inventory->inventory_type->type=='Delivery';

            if(!in_array($Inventory->InventoryDate, $InventoryUsedData) || $IsInventoryTypeDelivery){

                if($IsInventoryTypeDelivery){
                    $ByInventoryDate->DeliveryInventoryId = $Inventory->id;
                    $ByInventoryDate->DeliveryDate = $Inventory->InventoryDate;
                }else if($shouldAddInEnd){
                    $ByInventoryDate->EndDateInventoryId = $Inventory->id;
                    $ByInventoryDate->EndDate = $Inventory->InventoryDate;
                }else{
                    $ByInventoryDate->StartDateInventoryId = $Inventory->id;
                    $ByInventoryDate->StartDate = $Inventory->InventoryDate;
                }

                foreach ($Inventory->inventory_count as $InvCount){
                    $InventoryCountModelForReport = new InventoryCountModelForReport();
                    $InventoryCountModelForReport->ItemId = $InvCount->item->id;
                    $InventoryCountModelForReport->ItemName = $InvCount->item->name;
                    $InventoryCountModelForReport->ItemCount = $InvCount->item_count;

                    if($IsInventoryTypeDelivery){
                        if($ByInventoryDate->DeliveryCount!=null && array_key_exists($InventoryCountModelForReport->ItemId, $ByInventoryDate->DeliveryCount)){
                            $ByInventoryDate->DeliveryCount[$InventoryCountModelForReport->ItemId] += $InventoryCountModelForReport;
                        }else {
                            $ByInventoryDate->DeliveryCount[$InventoryCountModelForReport->ItemId] = $InventoryCountModelForReport;
                        }
                    }else if($shouldAddInEnd){
                        $ByInventoryDate->InventoryEndCount[$InventoryCountModelForReport->ItemId] = $InventoryCountModelForReport;
                    }else{
                        $ByInventoryDate->InventoryStartCount[$InventoryCountModelForReport->ItemId] = $InventoryCountModelForReport;
                    }
                }

                if(!$shouldAddInEnd && !$IsInventoryTypeDelivery){
                    $ByInventoryDate->GetInventoryCount(); //save used count

                    $CloneByInventoryDate = clone $ByInventoryDate;
                    $InventoryUsedData[$Inventory->InventoryDate] = $CloneByInventoryDate;

                    $TempByInventoryDate = new ByInventoryDate();
                    $TempByInventoryDate->EndDateInventoryId = $ByInventoryDate->StartDateInventoryId;

                    $TempByInventoryDate->EndDate = $ByInventoryDate->StartDate;

                    $TempByInventoryDate->InventoryEndCount = $ByInventoryDate->InventoryStartCount;
                    $TempByInventoryDate->InventoryUsedCount = [];
                    $ByInventoryDate = $TempByInventoryDate;

                }else{
                    $shouldAddInEnd = false;
                }

            }
        }
        return view('report.index')
            ->with(['InventoryData' => $InventoryUsedData]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
