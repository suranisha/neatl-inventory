<?php

namespace App\Http\Controllers\Item;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTag;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\View\View;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        $Tag = Tag::all()->where('isActive',true);

        return view('item.Tag')->with(['ActiveTag'=>$Tag]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreTag $request): \Illuminate\Http\RedirectResponse
    {
        //validation
        $validated = $request->validated();

        $Tag = new Tag();
        $Tag->name = $validated['name'];
        $Tag->save();
        return redirect()->route('tag.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): \Illuminate\Http\RedirectResponse
    {
        $Tag = Tag::findOrFail($id);
        $Tag->isActive = 0;
        $Tag->save();

        return redirect()->route('tag.index');
    }
}
