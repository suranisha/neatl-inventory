<?php

namespace App\Http\Controllers\Item;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreItem;
use App\Models\Category;
use App\Models\Item;
use App\Models\Tag;
use http\Exception;
use Illuminate\Http\Request;
use function Laravel\Prompts\error;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): \Illuminate\Contracts\View\View
    {
        $Item = Item::all()->where('isActive',true);
        $Categories = Category::all()->where('isActive',true);
        $Tags = Tag::all()->where('isActive',true);

        return view('item.item')->with([
            'ActiveItem' => $Item,
            'AllCategories' => $Categories,
            'AllTags' => $Tags,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreItem $request): \Illuminate\Http\RedirectResponse
    {
        //validation
        $validated = $request->validated();

        try {
            $Item = new Item();
            $Item->name = $validated['name'];
            $Item->category_id = $validated['category_id'];
            $Item->save();

            foreach ($request->tags as $tag){
                $Item->tags()->attach(Tag::findOrFail($tag));
            }

        } catch (\Exception $e) {
            report($e);
        }

        return redirect()->route('item.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id): \Illuminate\Http\RedirectResponse
    {
        $Item = Item::findOrFail($id);
        $Item->isActive = 0;
        $Item->save();

        return redirect()->route('item.index');
    }

    public function show(string $id)
    {
        $Item = Item::findOrFail($id);
        $Categories = Category::all()->where('isActive',true);
        $Tags = Tag::all()->where('isActive',true);

        $itemTags = $Item->Tags()->get();
        $ItemTagsIds = [];
        foreach ($itemTags as $itemTag){
            $ItemTagsIds[] = $itemTag->id;
        }

        return view('item.item-edit')->with([
            'Item' => $Item,
            'AllCategories' => $Categories,
            'AllTags' => $Tags,
            'ItemTagsIds' => $ItemTagsIds
        ]);
    }

    public function update(StoreItem $request, string $id)
    {
        //validation
        $validated = $request->validated();

        try {
            $Item = Item::findOrFail($id);
            $Item->name = $validated['name'];
            $Item->category_id = $validated['category_id'];
            $Item->save();

            $Item->RemoveAllTags();

            foreach ($request->tags as $tag){
                $Item->tags()->attach(Tag::findOrFail($tag));
            }

        } catch (\Exception $e) {
            report($e);
        }

        return redirect()->route('item.index');
    }
}
