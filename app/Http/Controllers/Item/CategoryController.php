<?php

namespace App\Http\Controllers\Item;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategory;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Categories = Category::all()->where('isActive',true);

        return view('item.category')->with(['ActiveCategories'=>$Categories]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreCategory $request)
    {
        //validation
        $validated = $request->validated();

        $Cat = new Category();
        $Cat->name = $validated['name'];
        $Cat->save();
        return redirect()->route('category.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $Category = Category::findOrFail($id);
        $Category->isActive = 0;
        $Category->save();

        return redirect()->route('category.index');
    }
}
