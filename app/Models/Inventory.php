<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    use HasFactory;

    public function inventory_type(){
        return $this->belongsTo('App\Models\Inventory_type');
    }

    public function inventory_count(){
        return $this->hasMany('App\Models\inventory_count');
    }
}
