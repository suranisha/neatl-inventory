<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Item;

class Tag extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    public function Items(){
        return $this->belongsToMany('App\Models\Tag', 'Item_tag');
    }

}
