<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class inventory_count extends Model
{
    use HasFactory;

    public function inventory(){
        return $this->belongsTo('App\Models\inventory');
    }

    public function item(){
        return $this->belongsTo('App\Models\item');
    }
}
