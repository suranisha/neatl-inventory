<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class InventoryCountModelForReport
{
    use HasFactory;

    public $ItemId;
    public $ItemName;
    public $ItemCount;


}
