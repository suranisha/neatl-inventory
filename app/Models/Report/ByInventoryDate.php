<?php

namespace App\Models\Report;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class ByInventoryDate
{
    use HasFactory;

    //    Start Vars
    public $StartDateInventoryId;
    public $StartDate;
    public $InventoryStartCount = [];

    //    End Vars
    public $EndDateInventoryId;
    public $InventoryEndCount = [];
    public $EndDate;

    //    Delivery Vars
    public $DeliveryInventoryId;
    public $DeliveryDate;
    public $DeliveryCount = [];

    //    Actual Vars
    public $InventoryUsedCount = [];

    public function GetInventoryCountModel($id, $name, $Count){
        $InventoryCountModel = new InventoryCountModelForReport();
        $InventoryCountModel->ItemId = $id;
        $InventoryCountModel->ItemName = $name;
        $InventoryCountModel->ItemCount = $Count;
        return $InventoryCountModel;
    }

    public function GetInventoryCount(){
        $AllItems = [];

        //Start -- doing this way to avoid reference to other object
        foreach($this->InventoryStartCount as $id=>$itemObj){
            $InventoryCountModel = $this->GetInventoryCountModel($id, $itemObj->ItemName, $itemObj->ItemCount);
            $AllItems[$id] = $InventoryCountModel;
        }

        //Delivery
        foreach($this->DeliveryCount as $id=>$itemObj){
            if(!array_key_exists($id, $AllItems)){
                $InventoryCountModel = $this->GetInventoryCountModel($id, $itemObj->ItemName, $itemObj->ItemCount);
                $AllItems[$id] = $InventoryCountModel;
            }else{
                $AllItems[$id]->ItemCount += $itemObj->ItemCount;
            }
        }

        //End Count
        foreach($this->InventoryEndCount as $id=>$itemObj){
            if(!array_key_exists($id, $AllItems)){
                $InventoryCountModel = $this->GetInventoryCountModel($id, $itemObj->ItemName, $itemObj->ItemCount);
                $AllItems[$id] = $InventoryCountModel;
            }else{
                $AllItems[$id]->ItemCount -= $itemObj->ItemCount;
            }
        }

        $this->InventoryUsedCount = $AllItems;
    }
}
