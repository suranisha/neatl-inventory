

<form method="POST" action="{{$action}}">
    @csrf
    @method('DELETE')
    <button href="javascript:void(0);"
            class="btn btn-icon btn-sm btn-danger-transparent rounded-pill" type="submit"><i
            class="ri-delete-bin-line"></i></button>
</form>
