@if($errors->any())
    <ul class="text-danger fs-15">
        @foreach($errors->all() as $error)
            <li>{{$error}} </li>
        @endforeach
    </ul>
@endif
