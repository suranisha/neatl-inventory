<div class="modal fade" id="AddCategory" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">New Category</h6>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <form action="{{route('category.store')}}" method="POST" class="row g-3 needs-validation">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="categoryName" class="col-form-label">Category Name:</label>
                        <input type="text" class="form-control" id="categoryName" name="name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal"
        data-bs-target="#AddCategory" data-bs-whatever="@mdo">ADD</button>
