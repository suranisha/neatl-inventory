<div class="modal fade" id="AddTag" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">New Item</h6>
                <button type="button" class="btn-close" data-bs-dismiss="modal"
                        aria-label="Close"></button>
            </div>
            <form action="{{route('item.store')}}" method="POST"
                  class="row g-3 needs-validation">
                @csrf
                <div class="modal-body">
                    <div class="mb-3">
                        <label for="itemName" class="col-form-label">Item Name<span class="text-danger">*</span></label>
                        <input type="text" class="form-control" id="itemName"
                               name="name" required>
                    </div>
                    <div class="mb-3">
                        <div class="card custom-card">
                            <div class="card-header justify-content-between">
                                <div class="card-title">
                                    Select Category<span class="text-danger">*</span>
                                </div>
                            </div>
                            <div class="card-body">
                                <select name="category_id" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example" required>
                                    <option selected=""></option>
                                    @foreach($AllCategories as $Category)
                                        <option value="{{$Category->id}}">{{$Category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="mb-3">
                        <div class="card-body">
                            <p class="fw-semibold mb-2">Select Tags</p>
                            <select class="form-control" name="tags[]" id="choices-multiple-remove-button" multiple>
                                @foreach($AllTags as $Tag)
                                    <option value="{{$Tag->id}}">{{$Tag->name}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary"
                            data-bs-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
