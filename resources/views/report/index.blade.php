@extends('layouts.master')

@section('styles')
@endsection

@section('content')
    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Empty Page</h1>
            </div>
            <!-- Page Header Close -->

            <!-- Start::row-1 -->
            <div class="row">
                @foreach($InventoryData as $Data)
                    <div class="col-xl-6">
                        <div class="card custom-card">
                            <p class="fw-medium fs-18 m-auto">{{date('M d Y', strtotime($Data->StartDate))}} - {{date('M d Y', strtotime($Data->EndDate))}}</p>
                            <div class="card-body">
                                <table class="table text-nowrap table-bordered">
                                    <thead>
                                    <tr>
                                        <th class="col-md-2" scope="col">Name</th>
                                        <th scope="col">Start</th>
                                        <th scope="col">Delivery</th>
                                        <th scope="col">Used</th>
                                        <th scope="col">End</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {{-- Get All Items from End Delivery--}}
                                    @foreach($Data->InventoryEndCount as $itemId => $itemObj)
                                        <tr>
                                            <td scope="row">{{$itemObj->ItemName}}</td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryStartCount))
                                                    {{number_format($Data->InventoryStartCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->DeliveryCount))
                                                    {{number_format($Data->DeliveryCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryUsedCount))
                                                    {{number_format($Data->InventoryUsedCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryEndCount))
                                                    {{number_format($Data->InventoryEndCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach

                                    {{-- Get rest of the Items from Count Delivery--}}
                                    @foreach($Data->InventoryUsedCount as $itemId => $itemObj)
                                        @if (array_key_exists($itemId, $Data->InventoryEndCount))
                                            @continue
                                        @endif
                                        <tr>
                                            <td scope="row">{{$itemObj->ItemName}}</td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryStartCount))
                                                    {{number_format($Data->InventoryStartCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->DeliveryCount))
                                                    {{number_format($Data->DeliveryCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryUsedCount))
                                                    {{number_format($Data->InventoryUsedCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryEndCount))
                                                    {{number_format($Data->InventoryEndCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    {{-- Get rest of the Items from Start--}}
                                    @foreach($Data->InventoryStartCount as $itemId => $itemObj)
                                        @if (array_key_exists($itemId, $Data->InventoryEndCount) || array_key_exists($itemId, $Data->InventoryUsedCount))
                                            @continue
                                        @endif
                                        <tr>
                                            <td scope="row">{{$itemObj->ItemName}}</td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryStartCount))
                                                    {{number_format($Data->InventoryStartCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->DeliveryCount))
                                                    {{number_format($Data->DeliveryCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryUsedCount))
                                                    {{number_format($Data->InventoryUsedCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                            <td>
                                                @if(array_key_exists($itemId, $Data->InventoryEndCount))
                                                    {{number_format($Data->InventoryEndCount[$itemId]->ItemCount)}}
                                                @else
                                                    {{__(0)}}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
            <!--End::row-1 -->

        </div>
    </div>
    <!-- End::app-content -->

@endsection

@section('scripts')
@endsection
