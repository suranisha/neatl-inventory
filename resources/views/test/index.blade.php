@extends('layouts.master')

@section('styles')

    <!-- Prism CSS -->
    <link rel="stylesheet" href="{{asset('build/assets/libs/prismjs/themes/prism-coy.min.css')}}">

    <!-- Tom Select Css -->
    <link rel="stylesheet" href="{{asset('build/assets/libs/tom-select/css/tom-select.default.min.css')}}">

@endsection

@section('content')

    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Form Select</h1>
                <div class="ms-md-1 ms-0">
                    <nav>
                        <ol class="breadcrumb mb-0">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Forms</a></li>
                            <li class="breadcrumb-item active d-inline-flex" aria-current="page">Form Select</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!-- Page Header Close -->

            <!-- Start:: row-2 -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-header justify-content-between">
                            <div class="card-title">
                                Select Sizes
                            </div>
                        </div>
                        <div class="card-body">
                            <select class="form-select form-select-sm mb-3" aria-label=".form-select-sm example">
                                <option selected="">Open this select menu</option>
                                <option value="1">One</option>
                                <option value="2">Two</option>
                                <option value="3">Three</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End:: row-2 -->

            <!-- Start:: row-4 -->
            <h6 class="fw-semibold mb-2">Choices:</h6>
            <div class="row">
                <div class="col-xl-6">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card custom-card">
                                <div class="card-header d-flex align-items-center justify-content-between">
                                    <h6 class="card-title">Multiple Select</h6>
                                </div>

                                <select class="form-control" name="choices-multiple-remove-button" id="choices-multiple-remove-button" multiple>
                                    <option value="Choice 1" selected>Choice 1</option>
                                    <option value="Choice 2">Choice 2</option>
                                    <option value="Choice 3">Choice 3</option>
                                    <option value="Choice 4">Choice 4</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End:: row-4 -->

        </div>
    </div>
    <!-- End::app-content -->

@endsection

@section('scripts')

    <!-- Prism JS -->
    <script src="{{asset('build/assets/libs/prismjs/prism.js')}}"></script>
    @vite('resources/assets/js/prism-custom.js')

    <!-- Tom Select JS -->
    <script src="{{asset('build/assets/libs/tom-select/js/tom-select.complete.min.js')}}"></script>
{{--    @vite('resources/assets/js/tom-select.js')--}}

    <script>
        (function () {
            "use strict";

            /* multi select with remove button */
            const multipleCancelButton = new Choices(
                '#choices-multiple-remove-button',
                {
                    allowHTML: true,
                    removeItemButton: true,
                }
            );
        })();
    </script>

    <!-- Internal Choices JS -->
    @vite('resources/assets/js/choices.js')

@endsection
