@extends('layouts.master')

@section('styles')
@endsection

@section('content')
    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Empty Page</h1>
            </div>
            <!-- Page Header Close -->

            <!-- Start::row-1 -->
            <form action="{{route('inventory.update', $inventory->id)}}" method="POST"
                  class="row g-3 needs-validation">
                @csrf
                @method('PATCH')
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-3 mb-3">
                                        <div class="col-xl-12 mb-3">
                                            <label class="form-label">Date</label>
                                            <h3>{{date('M d Y', strtotime($inventory->InventoryDate))}}</h3>
                                        </div>
                                    </div>
                                    <div class="col-md-9 mb-3 mt-4">
                                        @foreach($InventoryType as $InvType)
                                            <div class="form-check form-check-md mb-3">
                                                <input class="form-check-input" type="radio" name="inventoryType_id"
                                                       value="{{$InvType->id}}" required disabled
                                                        @if($InvType->type == $inventory->inventory_type->type)
                                                           checked="checked"
                                                        @endif
                                                >
                                                <label class="form-check-label" for="Radio-md">
                                                    {{$InvType->type}}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @foreach($ItemsByCategory as $Category=>$Items)
                    <div class="col-xl-12">
                        <div class="card custom-card">
                            <div class="card-body">
                                <div class="row">
                                    <h4 class="page-title fw-medium fs-24 mb-5">{{$Category}}</h4>
                                    <div class="row">
                                        @foreach($Items as $Item)
                                            <div class="col-md-3 mb-3">
                                                <label class="form-label">{{$Item->name}}</label>
                                                <input type="number" name="{{$Item->id}}" class="form-control"
                                                       placeholder=""
                                                       aria-label="Phone number"
                                                        @if(array_key_exists($Item->id, $invCountsByItemId))
                                                           value="{{$invCountsByItemId[$Item->id]->item_count}}"
                                                        @else
                                                            value="0"
                                                        @endif
                                                >
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- End::app-content -->

@endsection

@section('scripts')
@endsection
