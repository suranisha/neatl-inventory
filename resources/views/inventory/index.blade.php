@extends('layouts.master')

@section('styles')
@endsection

@section('content')
    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Empty Page</h1>
            </div>
            <!-- Page Header Close -->

            <!-- Start::row-1 -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <a href="{{route('inventory.create')}}" type="button" class="btn btn-secondary btn-wave btn-lg mb-3"> Add </a>
                                <table class="table text-nowrap">
                                    <thead class="table-th-primary">
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Date</th>
                                        <th scope="col">Type</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($Inventories as $inventory)
                                        <tr>
                                            <th scope="row">{{$loop->iteration}}</th>
                                            <td>{{date('M d Y', strtotime($inventory->InventoryDate))}}</td>
                                            <td>{{$inventory->inventory_type->type}}</td>
                                            <td>
                                                <div class="hstack gap-2 fs-15">
                                                    <x-form.edit href="{{route('inventory.edit', $inventory->id)}}"></x-form.edit>
                                                    <x-form.delete action="{{route('inventory.destroy', $inventory)}}"></x-form.delete>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End::row-1 -->

        </div>
    </div>
    <!-- End::app-content -->

@endsection

@section('scripts')
@endsection
