
@extends('layouts.custom-master')

@section('styles')
@endsection

@section('content')

    <div class="container-lg">
        <div class="row justify-content-center align-items-center authentication authentication-basic h-100">
            <div class="col-xxl-4 col-xl-5 col-lg-5 col-md-6 col-sm-8 col-12">
                <div class="card custom-card">
                    <div class="card-body">
                        <div class="authentication-cover">
                            <div class="aunthentication-cover-content">
                                <p class="h5 fw-bold mb-2 text-center">Register</p>
                                <div class="text-center">
                                    <p class="fs-14 text-muted mt-3">Already have an account? <a href="{{route('login')}}" class="text-primary fw-semibold">Sign In Here</a></p>
                                </div>
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="row gy-3">
                                        <div class="col-xl-12">
                                            <label for="name" class="form-label text-default op=8">{{ __('Name') }}</label>
                                            <input type="text" class="form-control form-control-lg @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus id="name" placeholder="Full Name">

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-12">
                                            <label for="email" class="form-label text-default op=8 ">{{ __('Email Address') }}</label>
                                            <input type="text" class="form-control form-control-lg @error('email') is-invalid @enderror" id="email" placeholder="Email" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-xl-12">
                                            <label for="password" class="form-label text-default op=8">{{ __('Password') }}</label>
                                            <div class="input-group">
                                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="password">

                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-xl-12 mb-2">
                                            <label for="password" class="form-label text-default op=8">{{ __('Confirm Password') }}</label>

                                            <div class="input-group">
                                                <input id="password-confirm" type="password" class="form-control form-control-lg @error('password-confirm') is-invalid @enderror" name="password_confirmation" required autocomplete="new-password" placeholder="confirm password">
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-xl-12 d-grid mt-2">
                                            <button type="submit" class="btn btn-lg btn-primary">
                                                {{ __('Register') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')



@endsection
