@extends('layouts.master')

@section('styles')

    <!-- Prism CSS -->
{{--    <link rel="stylesheet" href="{{asset('build/assets/libs/prismjs/themes/prism-coy.min.css')}}">--}}

    <!-- Tom Select Css -->
    <link rel="stylesheet" href="{{asset('build/assets/libs/tom-select/css/tom-select.default.min.css')}}">

@endsection

@section('content')
    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Items</h1>
            </div>
            <!-- Page Header Close -->

            <!-- Start::row-1 -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body">
                            <div class="col-md-12">
                                @include('components.error')
                                <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal"
                                        data-bs-target="#AddTag" data-bs-whatever="@mdo">ADD
                                </button>

                                @include('components.add-item')

                                <div class="card custom-card">
                                    <div class="card-header justify-content-between">
                                        <div class="card-title">
                                            {{__('Active')}}
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table text-nowrap table-striped-columns">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Category</th>
                                                    <th scope="col">Tags</th>
                                                    <th scope="col">Active</th>
                                                    <th scope="col"></th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($ActiveItem as $Item)
                                                    <tr>
                                                        <th scope="row">{{$loop->iteration}}</th>
                                                        <td>{{$Item->name}}</td>
                                                        <td class="text-center">{{$Item->category->name}}</td>
                                                        <td class="text-center">
                                                            @foreach($Item->tags as $Tag)
                                                                <a href="javascript:void(0);" class="btn btn-primary">{{$Tag->name}}</a>
                                                            @endforeach
                                                        </td>
                                                        <td class="text-center"><i
                                                                class="bx bxs-circle text-green fs-10 rounded-circle"></i>
                                                        </td>
                                                        <td><a href="{{route('item.show',$Item->id)}}" class="btn btn-warning rounded-pill btn-wave waves-effect waves-light">EDIT</a></td>
                                                        <td>
                                                            <form method="POST"
                                                                  action="{{route('item.destroy', $Item->id)}}">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit" class="btn btn-danger rounded-pill btn-wave waves-effect waves-light">DELETE</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-footer d-none border-top-0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End::row-1 -->

                    </div>
                </div>
                <!-- End::app-content -->

@endsection

@section('scripts')

    <!-- Prism JS -->
{{--    <script src="{{asset('build/assets/libs/prismjs/prism.js')}}"></script>--}}
{{--    @vite('resources/assets/js/prism-custom.js')--}}

    <!-- Tom Select JS -->
    <script src="{{asset('build/assets/libs/tom-select/js/tom-select.complete.min.js')}}"></script>
{{--    @vite('resources/assets/js/tom-select.js')--}}
    <script>
        (function () {
            "use strict";

            /* multi select with remove button */
            const multipleCancelButton = new Choices(
                '#choices-multiple-remove-button',
                {
                    allowHTML: true,
                    removeItemButton: true,
                }
            );
        })();
    </script>
    <!-- Internal Choices JS -->
{{--    @vite('resources/assets/js/choices.js')--}}
@endsection
