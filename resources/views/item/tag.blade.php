@extends('layouts.master')

@section('styles')
@endsection

@section('content')
    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Tags</h1>
            </div>
            <!-- Page Header Close -->

            <!-- Start::row-1 -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body">
                            <div class="col-md-12">
                                @include('components.error')
                                <button type="button" class="btn btn-primary mb-1" data-bs-toggle="modal"
                                        data-bs-target="#AddTag" data-bs-whatever="@mdo">ADD
                                </button>

                                <div class="modal fade" id="AddTag" tabindex="-1"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h6 class="modal-title" id="exampleModalLabel">New Tag</h6>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                            </div>
                                            <form action="{{route('tag.store')}}" method="POST"
                                                  class="row g-3 needs-validation">
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="mb-3">
                                                        <label for="tagName" class="col-form-label">Tag Name:</label>
                                                        <input type="text" class="form-control" id="tagName"
                                                               name="name">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"
                                                            data-bs-dismiss="modal">Close
                                                    </button>
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="card custom-card">
                                    <div class="card-header justify-content-between">
                                        <div class="card-title">
                                            {{__('Active')}}
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="table text-nowrap table-striped-columns">
                                                <thead>
                                                <tr>
                                                    <th scope="col">#</th>
                                                    <th scope="col">Name</th>
                                                    <th scope="col">Item Count</th>
                                                    <th scope="col">Active</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($ActiveTag as $Tag)
                                                    <tr>
                                                        <th scope="row">{{$loop->iteration}}</th>
                                                        <td>{{$Tag->name}}</td>
                                                        <td class="text-center">{{count($Tag->Items)}}</td>
                                                        <td class="text-center"><i
                                                                class="bx bxs-circle text-green fs-10 rounded-circle"></i>
                                                        </td>
                                                        <td>
                                                            <form method="POST"
                                                                  action="{{route('tag.destroy', $Tag->id)}}">
                                                                @csrf
                                                                @method('DELETE')
                                                                <button type="submit"
                                                                        class="btn btn-sm btn-danger btn-wave">
                                                                    <i class="ri-delete-bin-line align-middle me-2 d-inline-block"></i>Delete
                                                                </button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="card-footer d-none border-top-0">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End::row-1 -->

                    </div>
                </div>
                <!-- End::app-content -->

@endsection

@section('scripts')
@endsection
