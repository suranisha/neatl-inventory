@extends('layouts.master')

@section('styles')
    <link rel="stylesheet" href="{{asset('build/assets/libs/tom-select/css/tom-select.default.min.css')}}">
@endsection

@section('content')
    <!-- Start::app-content -->
    <div class="main-content app-content">
        <div class="container-fluid">

            <!-- Page Header -->
            <div class="d-md-flex d-block align-items-center justify-content-between my-4 page-header-breadcrumb">
                <h1 class="page-title fw-medium fs-24 mb-0">Empty Page</h1>
            </div>
            <!-- Page Header Close -->

            <!-- Start::row-1 -->
            <div class="row">
                <div class="col-xl-12">
                    <div class="card custom-card">
                        <div class="card-body col-xl-8 m-auto">

                            <div aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form action="{{route('item.update',$Item->id)}}" method="POST"
                                              class="row g-3 needs-validation p-5">
                                            @method('PUT')
                                            @csrf
                                            <div class="mb-3">
                                                <label for="itemName" class="col-form-label">Item Name<span class="text-danger">*</span></label>
                                                <input type="text" class="form-control" id="itemName" value="{{$Item['name']}}"
                                                       name="name" required>
                                            </div>
                                            <div class="mb-3">
                                                <div class="card custom-card">
                                                    <div class="card-header justify-content-between">
                                                        <div class="card-title">
                                                            Select Category<span class="text-danger">*</span>
                                                        </div>
                                                    </div>
                                                    <div class="card-body">
                                                        <select name="category_id" class="form-select form-select-sm mb-3" aria-label=".form-select-sm example" required>
                                                            <option selected=""></option>
                                                            @foreach($AllCategories as $Category)
                                                                <option {{ $Item->category_id == $Category->id? 'selected' : '' }}
                                                                    value="{{$Category->id}}">{{$Category->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mb-3">
                                                <div class="card-body">
                                                    <p class="fw-semibold mb-2">Select Tags</p>
                                                    <select class="form-control" name="tags[]" id="choices-multiple-remove-button" multiple>
                                                        @foreach($AllTags as $Tag)
                                                            <option {{in_array($Tag->id, $ItemTagsIds)? 'selected' : '' }}
                                                                value="{{$Tag->id}}">{{$Tag->name}}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!--End::row-1 -->

        </div>
    </div>
    <!-- End::app-content -->

@endsection

@section('scripts')
    <script src="{{asset('build/assets/libs/tom-select/js/tom-select.complete.min.js')}}"></script>
    <script>
        (function () {
            "use strict";

            /* multi select with remove button */
            const multipleCancelButton = new Choices(
                '#choices-multiple-remove-button',
                {
                    allowHTML: true,
                    removeItemButton: true,
                }
            );
        })();
    </script>
@endsection
