<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Item\ItemController;
use App\Http\Controllers\Item\CategoryController;
use App\Http\Controllers\Item\TagController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\ReportController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

Route::get('/', [ItemController::class, 'index'])
    ->middleware('auth');

Route::get('/test', [App\Http\Controllers\TestController::class, 'index'])
    ->middleware('auth');

Route::resource('/category', CategoryController::class)
    ->middleware('auth')
    ->only(['index', 'store', 'destroy']);

Route::resource('/tag', TagController::class)
    ->middleware('auth')
    ->only(['index', 'store', 'destroy']);

Route::resource('/item', ItemController::class)
    ->middleware('auth')
    ->only(['index', 'show', 'store', 'destroy', 'update']);

Route::resource('/inventory', InventoryController::class)
    ->middleware('auth')
    ->only(['index', 'create', 'store', 'destroy', 'edit', 'update']);

Route::resource('/report', ReportController::class)
    ->middleware('auth')
    ->only(['index']);

